#Pontos de Interesse

Aplicação que permite cadastrar localizações de pontos de interesse. Além disso é possível listar pontos de proximidade por distância. Foi desenvolvida seguindo o modelo RESTful com formato JSON.
Campos:

* nome: String. Nome do estabelecimento (Restaurante, Posto de combustível, Praça, etc)
* x: número inteiro positivo. Coordenada geográfica x
* y: número inteiro positivo. Coordenada geográfica y
* opened: String no formato HH:mm, ex: 18:00. Horário de abertura (Opcional). Para pontos como a Praça, que não possuem horário de fechamento, essa propriedade não é requerida
* closed: String no formato HH:mm, ex: 18:00. Horário de fechamento (Opcional). Para pontos como a Praça, que não possuem horário de fechamento, essa propriedade não é requerida

## Pré requisitos:

* [NodeJs](https://nodejs.org/en/download)
* [MongoDB](https://www.mongodb.com/download-center)

## Modificar configurações padrão
Há três arquivos de configuração na pasta config:

* default.json
* dev.json
* test.json

Permitem alterar a configuraçao para cada ambiente em específico.
A URL padrão para o banco de dados é: `mongodb://localhost:27017/pontos-de-interesse`. A porta padrão do servidor é `9090`.
Para modificar a URL do banco de dados edite a propriedade DBHost. Para alterar a porta do servidor modifique a propriedade serverPort, por exemplo:

	{
		"DBHost": "mongodb://localhost:31031/pontos-de-interesse",
		"serverPort": 8080
	}

## Instalação
Para realizar a instalação, basta realizar o clone do projeto, entrar na pasta do mesmo e executar o seguinte comando:

	npm install

Esse comando realizará o download das dependências necessárias para execução e testes do projeto. Para iniciar o servidor basta iniciar o comando:

	npm start

Para realizar os testes basta executar o seguinte comando:

	npm test

## Estrutura
Na raiz encontram-se os arquivos:

* package.json: nele estão as dependências da aplicação, de desenvolvimento e de testes, além de outras informações do projeto.
* server.js: responsável por configurar e executar a aplicação

Os arquivos da pasta config são encontrados os arquivos de configuração para cada ambiente. Na pasta src temos as pastas controllers, models, routes e services. Na pasta routes são definidas as rotas e os métodos do controller referentes a cada uma. Na pasta controller são realizadas validações e regras de negócio, e são chamados os metódos da camada service. Na pasta service é realizado o tratamento dos dados e comunicação com a camada de banco de dados.

## API REST

A API expõe os seguintes métodos:

URL           |Verbo          |Corpo da requisição(request body)  | Descrição
--------------|------------- |------------- | -------------
/ponto-de-interesse       |GET          |void|lista todos os pontos de interesse existentes na base de dados.
/ponto-de-interesse |POST|JSON| cadastra um novo ponto de interesse.
/ponto-de-interesse/{id} |GET|void| retorna um ponto de interesse a partir do id.
/ponto-de-interesse/{id}|DELETE|void| apaga um ponto de interesse a partir do id.
/ponto-de-interesse/{id} | PUT | JSON | atualiza um ponto de interesse.
/ponto-de-interesse/proximos|GET|void| busca pontos de interesse baseado nas coordenadas x e y, no horário (hr) e na distância máxima (mts).

### Exemplo de requisições:

#### Listar todos os pontos de interesse

	[GET] /pontos-de-interesse
	
Retorno:

	[
      {
	      "_id": "59bd8f0dd34541e209819693",
	      "nome": "Restaurante",
	      "x": 27,
	      "y": 12,
	      "opened": "12:00",
	      "closed": "18:00"
	   },
	      {
	      "_id": "59bd8f49d34541e209819694",
	      "nome": "Posto de combustível",
	      "x": 31,
	      "y": 18,
	      "opened": "08:00",
	      "closed": "18:00"
	   },
	      {
	      "_id": "59bd8f5ad34541e209819695",
	      "nome": "Praça",
	      "x": 15,
	      "y": 12
	   }
	]
	
#### Cadastrar ponto de interesse
	
	[POST] /pontos-de-interesse
	
Corpo da requisição: deve retornar uma mensagem de erro, porque X e Y não são inteiros:

		{
			"nome": "Shopping Center",
			"x" : 31.1,
			"y" : 18.2,
			"opened" : "08:00",
			"closed" : "18:00"
		}
		
Retorno:

	{
	   "message": "ponto-de-interesse validation failed",
	   "name": "ValidationError",
	   "errors":    {
	      "y":       {
	         "message": "`18.2` is not an integer value",
	         "name": "ValidatorError",
	         "properties":          {
	            "type": "user defined",
	            "message": "`{VALUE}` is not an integer value",
	            "path": "y",
	            "value": 18.2
	         },
	         "kind": "user defined",
	         "path": "y",
	         "value": 18.2
	      },
	      "x":       {
	         "message": "`31.1` is not an integer value",
	         "name": "ValidatorError",
	         "properties":          {
	            "type": "user defined",
	            "message": "`{VALUE}` is not an integer value",
	            "path": "x",
	            "value": 31.1
	         },
	         "kind": "user defined",
	         "path": "x",
	         "value": 31.1
	      }
	   }
	}
	
Corpo da requisição: deve retornar uma mensagem de erro, porque não foi passado o campo nome:

	{
		"x" : 31,
		"y" : 18,
		"opened" : "08:00",
		"closed" : "18:00"
	}
	
Retorno:

	{
	   "message": "ponto-de-interesse validation failed",
	   "name": "ValidationError",
	   "errors": {"nome":    {
	      "message": "Path `nome` is required.",
	      "name": "ValidatorError",
	      "properties":       {
	         "type": "required",
	         "message": "Path `{PATH}` is required.",
	         "path": "nome"
	      },
	      "kind": "required",
	      "path": "nome"
	   }}
	}
	
Corpo da requisição: deve retornar com sucesso

	{
		"nome": "Shopping Center",
		"x" : 31,
		"y" : 18,
		"opened" : "08:00",
		"closed" : "18:00"
	}
	
Retorno:

	{
	   "message": "Ponto de interesse adicionado com sucesso!",
	   "ponto":    {
	      "nome": "Shopping Center",
	      "x": 31,
	      "y": 18,
	      "opened": "08:00",
	      "closed": "18:00",
	      "_id": "59be6eb5b21dc47f33a7c3e0"
	   }
	}
	
#### Listar todos os pontos de interesse proximos

URL: Deve retornar uma mensagem de erro, pois não foi passado o parâmetro mts.

	[GET] /pontos-de-interesse?x=20&y=10&hr=19:00
	
Retorno:

	{
	   "message": "validation failed",
	   "name": "ValidationError",
	   "errors":    {
	      "message": "Path mts is required.",
	      "name": "ValidatorError",
	      "properties":       {
	         "type": "required",
	         "message": "Path mts is required.",
	         "path": "mts"
	      },
	      "kind": "required",
	      "path": "mts"
	   }
	}
	
URL: Deve retornar uma mensagem de erro, pois não foi passado o parâmetro hr.

	[GET] /pontos-de-interesse?mts=10&x=20&y=10
	
Retorno:

	{
	   "message": "validation failed",
	   "name": "ValidationError",
	   "errors":    {
	      "message": "Path hr is required.",
	      "name": "ValidatorError",
	      "properties":       {
	         "type": "required",
	         "message": "Path hr is required.",
	         "path": "hr"
	      },
	      "kind": "required",
	      "path": "hr"
	   }
	}
	
URL: Deve retornar os pontos dentro da distância especificada.

	[GET] /pontos-de-interesse?mts=10&x=20&y=10&hr=19:00
	
Retorno:

	[
      {
      "nome": "Praça",
      "funcionamento": "aberto"
	   },
	      {
	      "nome": "Restaurante",
	      "funcionamento": "fechado"
	   }
	]

#### Listar ponto de interesse por id

URL:

	[GET] /ponto-de-interesse/59be6eb5b21dc47f33a7c3e0
	
Retorno:

	[{
	   "_id": "59be6eb5b21dc47f33a7c3e0",
	   "nome": "Shopping Center",
	   "x": 31,
	   "y": 18,
	   "opened": "08:00",
	   "closed": "18:00"
	}]
	
#### Atualizar ponto de interesse por id
	
URL:

	[PUT] /ponto-de-interesse/59be6eb5b21dc47f33a7c3e0
	
Corpo:

	{
		"nome": "Lanchonete",
		"x" : 32,
		"y" : 19,
		"opened" : "19:00",
		"closed" : "23:00"
	}
	
Retorno:

	{
	   "message": "Ponto de interesse atualizado com sucesso!",
	   "ponto":    {
	      "n": 1,
	      "nModified": 1,
	      "ok": 1
	   }
	}
	
#### Excluir ponto de interesse por id
	
URL:

	[DELETE] /ponto-de-interesse/59be6eb5b21dc47f33a7c3e0
	
Retorno:
	
	{
	   "message": "Ponto de interesse excluído com sucesso!",
	   "ponto":    {
	      "n": 1,
	      "ok": 1
	   }
	}