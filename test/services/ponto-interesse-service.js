process.env.NODE_ENV = 'test';

var PontoDeInteresse = require('../../src/models/ponto-de-interesse');

const chai = require('chai');
const service = require('../../src/services/ponto-interesse-service');
const server = require('../../server');
const should = chai.should();

var ponto1 = {nome : "Restaurante",	x : 27, y : 12,	opened : "12:00",closed : "18:00"};
var ponto3 = {nome : "Restaurante", x : 27, y : 12};
var pontoSemNome = {x : 27, y : 12, opened : "12:00",closed : "18:00"};
var pontoXNaoInteger = {nome : "Praça", x : 27.1, y : 12};
var pontoYNaoInteger = {nome : "Praça", x : 27, y : 12.1};
var pontoXNegativo = {nome : "Praça", x : -27, y : 12};
var pontoYNegativo = {nome : "Praça", x : 27, y : -12};

beforeEach(function(done) {
    PontoDeInteresse.remove({}, function(error, pontos) {
	done();
    });
});

describe('PontoDeInteresseService.find', function() {
    it('Deve retornar vazio', function(done) {
	service.fetch('', (error, pontos) => {
	    pontos.length.should.be.eql(0);
	    done();
	});
    });

    it('Deve retornar 1 ponto de interesse', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    service.fetch('', (error, pontos) => {
		pontos.length.should.be.eql(1);
		pontos[0].should.have.property('nome');
		pontos[0].should.have.property('x');
		pontos[0].should.have.property('y');
		pontos[0].should.have.property('opened');
		pontos[0].should.have.property('closed');
		done();
	    });
	});
    });

    it('Deve retornar 1 ponto de interesse sem os campos opened e closed', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto3);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    service.fetch('', (error, pontos) => {
		pontos.length.should.be.eql(1);
		pontos[0].should.have.property('nome');
		pontos[0].should.have.property('x');
		pontos[0].should.have.property('y');
		should.equal(pontos[0].opened, undefined);
		should.equal(pontos[0].closed, undefined);
		done();
	    });
	});
    });
});

describe('PontoDeInteresseService.save', function() {
    it('Não deve salvar, porque nao foi passado o campo nome', function(done) {
	ponto1
	service.save(pontoSemNome, (error, pontos) => {
	    error.errors.should.have.property('nome');
	    error.errors.nome.should.have.property('kind').eql('required');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo x como integer', function(done) {
	service.save(pontoXNaoInteger, (error, pontos) => {
	    error.errors.should.have.property('x');
	    error.errors.x.should.have.property('kind').eql('user defined');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo y como integer', function(done) {
	service.save(pontoYNaoInteger, (error, pontos) => {
	    error.errors.should.have.property('y');
	    error.errors.y.should.have.property('kind').eql('user defined');
	    done();
	});
    });
    
    it('Não deve salvar, porque nao foi passado o campo x como integer positivo', function(done) {
	service.save(pontoXNegativo, (error, pontos) => {
	    error.errors.should.have.property('x');
	    error.errors.x.should.have.property('kind').eql('min');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo y como integer positivo', function(done) {
	service.save(pontoYNegativo, (error, pontos) => {
	    error.errors.should.have.property('y');
	    error.errors.y.should.have.property('kind').eql('min');
	    done();
	});
    });

    it('Deve salvar 1 ponto de interesse', function(done) {
	service.save(ponto1, (error, ponto) => {
	    ponto.should.have.property('nome');
	    ponto.should.have.property('x');
	    ponto.should.have.property('y');
	    ponto.should.have.property('opened');
	    ponto.should.have.property('closed');
	    done();
	});
    });

    it('Deve salvar 1 ponto de interesse sem os campos opened e closed', function(done) {
	service.save(ponto3, (error, ponto) => {
	    ponto.should.have.property('nome');
	    ponto.should.have.property('x');
	    ponto.should.have.property('y');
	    should.equal(ponto.opened, undefined);
	    should.equal(ponto.closed, undefined);
	    done();
	});
    });
});

describe('PontoDeInteresseService.update', function() {
    it('Deve atualizar o ponto de interesse', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, (error, ponto) => {
	    ponto.nome = 'Shopping Center';
	    service.update(ponto._id, ponto, (error, ponto) => {
		ponto.n.should.eql(1);
		ponto.nModified.should.eql(1);
		ponto.ok.should.eql(1);
		done();
	    });    
	});

    });
});

describe('PontoDeInteresseService.delete', function() {
    it('Deve remover o ponto de interesse', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, (error, ponto) =>{
	    service.delete(ponto._id, (error, ponto) => {
		ponto.result.n.should.eql(1);
		ponto.result.ok.should.eql(1);
		done();
	    });    
	});

    });
});