process.env.NODE_ENV = 'test';

describe('Service', function() {
    require('./services/ponto-interesse-service');
});

describe('API', function() {
    require('./api/ponto-interesse-api');
});