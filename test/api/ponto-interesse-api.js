process.env.NODE_ENV = 'test';

var PontoDeInteresse = require('../../src/models/ponto-de-interesse');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const should = chai.should();

chai.use(chaiHttp);

const ponto1 = {nome : "Restaurante", x : 27, y : 12, opened : "12:00",closed : "18:00"};
const ponto1Opened11 = {nome : "Restaurante", x : 27, y : 12, opened : "11:00",closed : "18:00"};
const ponto2 = {nome : "Posto de combustível", x : 31, y : 18, opened : "08:00", closed : "18:00"};
const ponto3 = {nome : "Praça", x : 15, y : 12};
const pontoSemNome = {x : 27, y : 12, opened : "12:00",closed : "18:00"};
const getProximosInput = {x : 20, y : 10, mts : "10", hr : "19:00"};
const semNome = {x : 27, y : 12, opened : "12:00",closed : "18:00"};
const semMts = {x : 20, y : 10, hr : "19:00"};
const semHr = {x : 20, y : 10, mts : 10};
const pontoXNaoInteger = {nome : "Praça", x : 27.1, y : 12};
const pontoYNaoInteger = {nome : "Praça", x : 27, y : 12.1};
const pontoXNegativo = {nome : "Praça", x : -27, y : 12};
const pontoYNegativo = {nome : "Praça", x : 27, y : -12};
const serviceURL = '/ponto-de-interesse/';

beforeEach(function(done) {
    PontoDeInteresse.remove({}, function(error, pontos) {
	done();
    });
});

describe('/GET /ponto-de-interesse', function() {
    it('Deve retornar vazio', function(done) {
	chai.request(server).get(serviceURL).end(
		function(error, res) {
		    res.should.have.status(200);
		    res.body.should.be.a('array');
		    res.should.be.json;
		    res.body.length.should.be.eql(0);
		    done();
		});
    });

    it('Deve retornar um ponto de interesse sem os campos open e closed', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto3);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    chai.request(server).get(serviceURL).end(
		    function(error, res) {
			res.should.have.status(200);
			res.body.should.be.a('array');
			res.should.be.json;
			res.body.length.should.be.eql(1);
			res.body[0].should.not.have.property('opened');
			res.body[0].should.not.have.property('closed');
			done();
		    });
	});
    });
});

describe('/POST /ponto-de-interesse', function() {
    it('Não deve salvar, porque nao foi passado o nome', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(semNome)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.should.have.property('nome');
	    res.body.errors.nome.should.have.property('kind').eql('required');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo x como integer', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(pontoXNaoInteger)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.should.have.property('x');
	    res.body.errors.x.should.have.property('kind').eql('user defined');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo y como integer', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(pontoYNaoInteger)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.should.have.property('y');
	    res.body.errors.y.should.have.property('kind').eql('user defined');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo x como integer positivo', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(pontoXNegativo)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.should.have.property('x');
	    res.body.errors.x.should.have.property('kind').eql('min');
	    done();
	});
    });

    it('Não deve salvar, porque nao foi passado o campo y como integer positivo', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(pontoYNegativo)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.should.have.property('y');
	    res.body.errors.y.should.have.property('kind').eql('min');
	    done();
	});
    });

    it('Deve Criar um ponto de interesse', function(done) {
	chai.request(server)
	.post(serviceURL)
	.send(ponto1)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('message').eql('Ponto de interesse adicionado com sucesso!');
	    res.body.ponto.should.have.property('nome');
	    res.body.ponto.should.have.property('x');
	    res.body.ponto.should.have.property('y');
	    res.body.ponto.should.have.property('opened');
	    res.body.ponto.should.have.property('closed');
	    done();
	});
    });
});

describe('/GET /ponto-de-interesse/:id', function() {
    it('Deve retornar um ponto de interesse com o id passado', function(done) {
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    chai.request(server)
	    .get(serviceURL + ponto._id)
	    .end(function(error, res) {
		res.should.be.a('object');
		res.body[0].should.have.property('nome');
		res.body[0].should.have.property('x');
		res.body[0].should.have.property('y');
		res.body[0].should.have.property('opened');
		res.body[0].should.have.property('closed');
		res.body[0].should.have.property('_id').eql(ponto._id.toString());
		done();
	    });
	});
    });
});

describe('/PUT /ponto-de-interesse/:id', function(){
    it('Deve atualizar o ponto de interesse cujo id foi passado', function(done){
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    chai.request(server)
	    .put(serviceURL + ponto._id)
	    .send(ponto1Opened11)
	    .end(function(error, res){
		res.should.have.status(200);
		res.body.should.be.a('object');
		res.body.should.have.property('message').eql('Ponto de interesse atualizado com sucesso!');
		res.body.ponto.should.have.property('n').eql(1);
		res.body.ponto.should.have.property('nModified').eql(1);
		res.body.ponto.should.have.property('ok').eql(1);
		done();
	    });
	});
    });
});

describe('/DELETE /ponto-de-interesse/:id', function(){
    it('Deve excluir o ponto de interesse cujo id foi passado', function(done){
	const pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	    chai.request(server)
	    .delete(serviceURL + ponto._id)
	    .end(function(error, res){
		res.should.have.status(200);
		res.body.should.be.a('object');
		res.body.should.have.property('message').eql('Ponto de interesse excluído com sucesso!');
		done();
	    });
	});
    });
});

describe('/POST /ponto-de-interesse/proximos', function() {
    it('Deve retornar uma mensagem de validação, porque nao foi passado o campo mts', function(done) {
	chai.request(server)
	.get(serviceURL + '/proximos' + '?' + semMts.x + '&' + semMts.y + '&' + semMts.hr)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.path.should.eql('mts');
	    res.body.errors.should.have.property('kind').eql('required');
	    done();
	});
    });

    it('Deve retornar uma mensagem de validação, porque nao foi passado o campo hr', function(done) {
	chai.request(server)
	.get(serviceURL + '/proximos' + '?x=' + semHr.x + '&y=' + semHr.y + '&mts=' + semHr.mts)
	.end(function(error, res) {
	    res.should.have.status(200);
	    res.should.be.json;
	    res.body.should.be.a('object');
	    res.body.should.have.property('errors');
	    res.body.errors.path.should.eql('hr');
	    res.body.errors.should.have.property('kind').eql('required');
	    done();
	});
    });

    it('Deve retornar a Praça com status aberto e o Restaurante com status fechado', function(done) {
	let pontoDeInteresse = new PontoDeInteresse(ponto1);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	});
	pontoDeInteresse = new PontoDeInteresse(ponto2);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {
	});
	pontoDeInteresse = new PontoDeInteresse(ponto3);
	pontoDeInteresse.save(pontoDeInteresse, function(error, ponto) {

	    chai.request(server)
	    .get(serviceURL + '/proximos' + '?x=' + getProximosInput.x + '&y=' + getProximosInput.y + '&mts=' + getProximosInput.mts + '&hr=' + getProximosInput.hr)
	    .end(function(error, res) {
		res.should.have.status(200);
		res.should.be.json;
		res.body.should.be.a('array');
		res.body.length.should.be.eql(2);
		res.body[0].nome.should.be.eql("Praça");
		res.body[0].funcionamento.should.be.eql("aberto");
		res.body[1].nome.should.be.eql("Restaurante");
		res.body[1].funcionamento.should.be.eql("fechado");
		done();
	    });
	});
    });
});