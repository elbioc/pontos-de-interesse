const express = require('express');
let app = express();
const mongoose = require('mongoose');
const config = require('config');
const port = config.serverPort;
const bodyParser = require('body-parser');
const route = require ('./src/routes/ponto-de-interesse');

const options = {
	server : {
	    socketOptions : {
		keepAlive : 1,
		connectTimeoutMS : 30000
	    }
	},
	replset : {
	    socketOptions : {
		keepAlive : 1,
		connectTimeoutMS : 30000
	    }
	}
};

mongoose.connect(config.DBHost, options);
let db = mongoose.connection;
db.on('error', console.error.bind(console,
'Erro ao conectar com a Base de Dados....: '));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));
app.use("/ponto-de-interesse", route);

app.listen(port);
console.log("Aplicação executando na porta " + port);

module.exports = app;