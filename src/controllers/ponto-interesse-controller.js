const service = require('../services/ponto-interesse-service');
let PontoDeInteresseSchema = require('../models/ponto-de-interesse')

let controller = {};

controller.listAll = (req, res) => {

    service.fetch(req.query, (error, pontos) => {
	if (error) {
	    res.send(error);
	}
	res.json(pontos);
    });
}

controller.save = (req, res) => {
    let novoPontoDeInteresse = req.body;
    service.save(novoPontoDeInteresse, (error, ponto) => {
	if(error) {
	    res.send(error);
	} else {
	    res.json({ message: "Ponto de interesse adicionado com sucesso!", ponto });
	}
    });
}

controller.getPontoDeInteresse = (req, res) => {
    service.fetch({_id: req.params.id},  (error, ponto) => {
	if(error) {
	    res.send(error);
	} else {
	    res.json(ponto);
	}
    });
}

controller.delete = (req, res) => {
    service.delete(req.params.id, (error, ponto) => {
	if (error) {
	    res.send(error);
	}
	res.json({ message: "Ponto de interesse excluído com sucesso!", ponto });

    });
}

controller.update = (req, res) => {
    service.update(req.params.id, req.body, (error, ponto) => {
	if (error) {
	    res.send(error);
	}
	res.json({ message: "Ponto de interesse atualizado com sucesso!", ponto });
    })
}


controller.getProximos = (req, res) => {
    service.fetch('', (error, pontos) => {
	if(error){
	    res.send(error);
	}
	if(!req.query.mts){
	    res.json(generateRequiredErrorMessage('mts'));
	} else if(!req.query.hr){
	    res.send(generateRequiredErrorMessage('hr'));
	} else {
	    pontos = filterAndSort(pontos, req);

	    res.json(generateRetornoStatus(pontos, req));
	}
    });
}

const filterAndSort = (pontos, req) => {
    pontos = pontos.filter(function(ponto) {
	return getDistancia(req.query.x, req.query.y, ponto.x, ponto.y) <= req.query.mts;
    });
    pontos = pontos.sort(function(pontoA, pontoB) {
	return getDistancia(req.query.x, req.query.y, pontoA.x, pontoA.y) 
	- getDistancia(req.query.x, req.query.y, pontoB.x, pontoB.y);
    });
    return pontos;
}

const getDistancia = (x1, y1, x2, y2) => {
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

const generateRetornoStatus = (pontos, req) => {
    const today = new Date();
    let pontosRetorno = [];

    pontos.forEach(function(ponto){
	if(ponto.closed){
	    let dateSplit = ponto.closed.split(':');
	    var date1 = new Date(today.getFullYear(), today.getMonth(), today.getDate(), dateSplit[0], dateSplit[1], 0);
	    dateSplit = req.query.hr.split(':');
	    var date2 = new Date(today.getFullYear(), today.getMonth(), today.getDate(), dateSplit[0], dateSplit[1], 0);
	    pontosRetorno.push({nome:ponto.nome, funcionamento:date2 >= date1 ? 'fechado': 'aberto'});
	} else {
	    pontosRetorno.push({nome:ponto.nome, funcionamento:'aberto'});
	}
    });
    return pontosRetorno;
}

const generateRequiredErrorMessage = (propertyName) => {
    return {"message": "validation failed","name": "ValidationError","errors":
    {"message": "Path " + propertyName + " is required.","name": "ValidatorError","properties":
    {"type": "required","message": "Path " + propertyName + " is required.","path": propertyName},
    "kind": "required","path": propertyName}};
}


module.exports = controller;