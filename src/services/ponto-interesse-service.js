const PontoDeInteresseSchema = require('../models/ponto-de-interesse');
const ObjectId = require('mongoose').Types.ObjectId;

let service = {};

service.fetch = (params, callback) => {
    if(params._id) {
	params._id = new ObjectId(params._id);
    }

    PontoDeInteresseSchema.find(params, (error, pontoDeInteresses) => {
	if(error) {
	    callback(error);
	} else {
	    callback(null, pontoDeInteresses);
	}
    });
}

service.save = (data, callback) => {
    const pontoDeInteresse = new PontoDeInteresseSchema(data);
    pontoDeInteresse.save(pontoDeInteresse, (error, ponto) => {
	if(error) {
	    callback(error);
	} else {
	    callback(null, ponto)
	}
    });
}

service.update = (id, data, callback) => {
    id = new ObjectId(id);
    PontoDeInteresseSchema.update({_id: id},{'$set': data}, (error, ponto) => {
	if(error) {
	    callback(error);
	} else {
	    callback(null, ponto);
	}
    });
}

service.delete = (id, callback) => {
    PontoDeInteresseSchema.remove({_id: ObjectId(id)}, (error, ponto) => {
	if(error) {
	    callback(error);
	} else {
	    callback(null, ponto);
	}
    });
}

module.exports = service;