var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PontoDeInteresseSchema = new Schema({
    nome : {
	type : String,
	required : true
    },
    x : {
	type : Number,
	required : true,
	min : 0,
	validate : {
	    validator : Number.isInteger,
	    message : '`{VALUE}` is not an integer value'
	}
    },
    y : {
	type : Number,
	required : true,
	min : 0,
	validate : {
	    validator : Number.isInteger,
	    message : '`{VALUE}` is not an integer value'
	}
    },
    opened : {
	type : String
    },
    closed : {
	type : String
    }
}, {
    versionKey : false
});

module.exports = mongoose.model('ponto-de-interesse', PontoDeInteresseSchema);