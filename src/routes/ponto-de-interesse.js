const express = require('express');
const router = express();
const controller = require('../controllers/ponto-interesse-controller');

router.route('/')
    .get(controller.listAll)
    .post(controller.save);

router.route('/proximos')
.get(controller.getProximos);
 
router.route('/:id')
    .get(controller.getPontoDeInteresse)
    .delete(controller.delete)
    .put(controller.update);
	
module.exports = router;
